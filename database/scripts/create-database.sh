#!/bin/bash

set -e

APP_USER=${APP_USER:-bot}
APP_PASSWORD=${APP_PASSWORD:-bot}
APP_DATABASE="passwords"

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<EOF 
CREATE USER $APP_USER PASSWORD '$APP_PASSWORD';
CREATE DATABASE $APP_DATABASE;
GRANT CREATE ON DATABASE $APP_DATABASE TO $APP_USER;
EOF

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$APP_DATABASE" <<EOF 
CREATE TABLE passwords (
  id SERIAL PRIMARY KEY,
  tgUsername text NOT NULL,
  service text NOT NULL,
  login text NOT NULL,
  password text NOT NULL
);
ALTER TABLE passwords OWNER TO $APP_USER;
EOF