package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	_ "github.com/jackc/pgx/v5/stdlib"
	commandhandler "gitlab.com/anton.kravtsov.31/vk-probation-bot/server/internal/command_handler"
)

var (
	BotToken   = "5974531911:AAEl9VxEDYedQdYtmgKM29DmPCzkZZUtCUU"
	WebhookURL = "provide in docker-compose"
	isDebug    = false
)

func startBot(ctx context.Context) error {
	WebhookURL := os.Getenv("WEBHOOK_URL")

	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		return fmt.Errorf("NewBotApi failed: %s", err)
	}
	bot.Debug = true
	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)

	wh, err := tgbotapi.NewWebhook(WebhookURL)
	if err != nil {
		return fmt.Errorf("NewWebhook failed: %s", err)
	}

	_, err = bot.Request(wh)
	if err != nil {
		return fmt.Errorf("SetWebhook failed: %s", err)
	}

	updates := bot.ListenForWebhook("/")

	http.HandleFunc("/state", func(w http.ResponseWriter, r *http.Request) {
		_, err := w.Write([]byte("all is working"))
		if err != nil {
			panic(err)
		}
	})

	port := os.Getenv("PORT")
	if port == "" {
		port = "8081"
	}

	go func() {
		log.Fatal("http err:", http.ListenAndServe(":"+port, nil))
	}()

	if isDebug {
		fmt.Println("start listen :" + port)
	}

	db, err := sql.Open("pgx", os.Getenv("DATABASE_URL"))
	if err != nil {
		log.Fatalf("failed to connect to db: %v", err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatalf("could not ping db: %v", err)
	}

	commandHandler := new(commandhandler.CommandHandler)
	commandHandler.Init(bot, db)

	for update := range updates {
		if isDebug {
			log.Printf("\t upd: %#v\n\n", update)
		}
		if update.Message == nil {
			continue
		}
		err := commandHandler.Handle(*update.Message)
		if err != nil {
			return fmt.Errorf("error occurred: %w", err)
		}
	}

	return nil
}

func main() {
	flag.BoolVar(&isDebug, "debug", false, "debug logging")
	flag.Parse()

	err := startBot(context.Background())
	if err != nil {
		panic(err)
	}
}
