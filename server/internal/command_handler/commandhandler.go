package commandhandler

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	_ "github.com/jackc/pgx/v5/stdlib"
)

var DeleteTimeSeconds = 5

type CommandHandler struct {
	bot              *tgbotapi.BotAPI
	commands         map[string]func(msg tgbotapi.Message) error
	knownUsersChatID map[string]int64
	db               *sql.DB
}

func (ch *CommandHandler) Init(bot *tgbotapi.BotAPI, db *sql.DB) {
	obtainedValue, err := strconv.Atoi(os.Getenv("MSG_DELETE_TIME"))
	if err != nil {
		log.Fatal("error on init, cant convert env variable: ", err)
	}
	DeleteTimeSeconds = obtainedValue

	ch.bot = bot
	ch.commands = map[string]func(msg tgbotapi.Message) error{
		"^/set ([^/]+); ([^/]+); ([^/]+)$": ch.handleSetCommand,
		"^/get ([^/]+)$":                   ch.handleGetCommand,
		"^/del ([^/]+)$":                   ch.handleDelCommand,
		"^/start$":                         ch.handleStartCommand,
	}
	ch.knownUsersChatID = make(map[string]int64)
	ch.db = db
}

func (ch *CommandHandler) Handle(msg tgbotapi.Message) error {
	ch.knownUsersChatID[msg.From.UserName] = msg.From.ID

	for pattern, command := range ch.commands {
		match, err := regexp.Match(pattern, []byte(msg.Text))
		if err != nil {
			return fmt.Errorf("error while matching regex patter: %w", err)
		}
		if !match {
			continue
		}

		err = command(msg)
		if err != nil {
			return fmt.Errorf("error while handling command: %w", err)
		}
		return nil

	}

	ch.deleteMsgAfterTime(msg.MessageID, ch.knownUsersChatID[msg.From.UserName])
	return ch.sendMessage(msg.From.UserName, "Неизвестная команда!", true)
}

func (ch *CommandHandler) sendMessage(toUsername, msgText string, deleteAfter bool) error {
	msg := tgbotapi.NewMessage(ch.knownUsersChatID[toUsername], msgText)
	m, err := ch.bot.Send(msg)
	if err != nil {
		return fmt.Errorf("error while sending message: %w", err)
	}

	if deleteAfter {
		ch.deleteMsgAfterTime(m.MessageID, ch.knownUsersChatID[toUsername])
	}

	return nil
}

func (ch *CommandHandler) deleteMsgAfterTime(msgID int, chatID int64) {
	delMsg := tgbotapi.NewDeleteMessage(chatID, msgID)
	go func() {
		t := time.NewTicker(time.Second * time.Duration(DeleteTimeSeconds))
		<-t.C

		_, err := ch.bot.Send(delMsg)
		if err != nil {
			log.Println("Cant delete msg: ", err)
		}
	}()
}

func (ch *CommandHandler) handleStartCommand(msg tgbotapi.Message) error {
	startText := `Добро пожаловать в ваш личный хранитель паролей!
	Команды:
	/set <service name>; <login>; <password>
	/get <service name>
	/del <service name`
	return ch.sendMessage(msg.From.UserName, startText, false)
}

func (ch *CommandHandler) handleSetCommand(msg tgbotapi.Message) error {
	ch.deleteMsgAfterTime(msg.MessageID, ch.knownUsersChatID[msg.From.UserName])

	txt := strings.Split(msg.Text, ";")
	servName := strings.Split(txt[0], " ")[1]
	login := txt[1]
	passwd := txt[2]

	log.Println("adding new entry in passwords: ", servName, login, passwd)
	_, err := ch.db.Exec("INSERT INTO passwords (tgUsername, service, login, password) VALUES ($1, $2, $3, $4)",
		&msg.From.UserName, &servName, &login, &passwd)
	if err != nil {
		return fmt.Errorf("cant insert into table: %w", err)
	}

	return ch.sendMessage(msg.From.UserName, "Пароль и логин успешно добавлены!", true)
}

func (ch *CommandHandler) handleGetCommand(msg tgbotapi.Message) error {
	ch.deleteMsgAfterTime(msg.MessageID, ch.knownUsersChatID[msg.From.UserName])

	servName := strings.Split(msg.Text, " ")[1]

	rows, err := ch.db.Query("SELECT login, password FROM passwords WHERE service = $1 AND tgUsername = $2",
		&servName, &msg.From.UserName)
	if err != nil {
		return fmt.Errorf("cant select: %w", err)
	}

	if !rows.Next() {
		return ch.sendMessage(msg.From.UserName, "Нет сохраненных паролей для этого сервиса", true)
	}

	responseText := "Сохраненные пароли для сервиса " + servName + ":"
	for {
		var login, password *string
		err := rows.Scan(&login, &password)
		if err != nil {
			return fmt.Errorf("cant scan row: %w", err)
		}
		responseText += "\n" + *login + ": " + *password

		if !rows.Next() {
			break
		}
	}

	return ch.sendMessage(msg.From.UserName, responseText, true)
}

func (ch *CommandHandler) handleDelCommand(msg tgbotapi.Message) error {
	ch.deleteMsgAfterTime(msg.MessageID, ch.knownUsersChatID[msg.From.UserName])

	servName := strings.Split(msg.Text, " ")[1]
	_, err := ch.db.Exec("DELETE FROM passwords WHERE service = $1 AND tgUsername = $2", &servName, msg.From.UserName)
	if err != nil {
		return fmt.Errorf("cant execute delete on db: %w", err)
	}

	return ch.sendMessage(msg.From.UserName, "Все пароли для этого сервиса успешно удалены!", true)
}
